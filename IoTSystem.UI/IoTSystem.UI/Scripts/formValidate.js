﻿
function FormValidate() {
    var success = true;
    $(".warning_label").remove();


    function AddErrorLabel(msg, id) {
        $('<label class="warning_label">' + msg + '</label>').insertAfter($("#" + id).closest(".input_container"));
        success = false;
    }

    $('.custom_form *').filter(':input').each(function () {
        var value = $(this).val();
        if (value.length === 0 && !$(this).prop('required')) {
            //do nothing for the moment
        }
        else {
            if (this.type === "text") {
                if (value.length === 0) {
                    AddErrorLabel('Please enter some text', this.id);
                }
                if (value.length > 200) {
                    AddErrorLabel('The text you entered exceeds 200 characters', this.id);
                }
            }
            if (this.type === "textarea") {
                if (value.length === 0) {
                    AddErrorLabel('Please enter some text', this.id);
                }
                if (value.length > 1000) {
                    AddErrorLabel('The text you entered exceeds 1000 characters', this.id);
                }
            }
            if (this.type === "email") {
                if (value.length > 254) {
                    AddErrorLabel('The email address you entered exceeds 254 characters', this.id);
                }
                if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(value)) {
                    AddErrorLabel('Please enter a valid email address', this.id);
                }
            }
            if (this.type === "tel") {
                if (value.length > 50) {
                    AddErrorLabel('The phone number you entered exceeds 50 characters', this.id);
                }
                if (!/([0-9\s\-]{7,})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/.test(value)) {
                    AddErrorLabel('Please enter a valid phone number', this.id);
                }
            }
            if (this.type === "number") {
                if (value.length > 100) {
                    AddErrorLabel('The number you entered exceeds 100 characters', this.id);
                }
                if (!/^\d*$/.test(value)) {
                    AddErrorLabel('Please enter a valid number', this.id);
                }
            }

            if (this.type === "checkbox") {
                if ($(this).prop("checked") === false && $(this).prop('required')) {
                    $('<label class="warning_label checkbox_raised">' + "Please tick the box" + '</label>').insertAfter($("#" + this.id).closest("div"));
                    success = false;
                }
            }
        }
    });

    if (success === false) {
        return false;
    }
}
function BlurFailure(id) {
    $("#" + id).parent().removeClass("form_success");
    $("#" + id).parent().addClass("form_failure");
}
function BlurSuccess(id) {
    $("#" + id).parent().removeClass("form_failure");
    $("#" + id).parent().addClass("form_success");
}

function RegCheck(id, str, reg, length) {
    var error = false;
    if (!reg.test(str)) {
        error = true;
    }
    if (str.length > length) {
        error = true;
    }
    if (!error) {
        $("#" + this.id).closest(".input_container").removeClass("warning_label");
        BlurSuccess(id);
    }
    else if (error) {
        BlurFailure(id);
    }
}

$('.form-group :input').blur(function () {
    alert("blur");
    var value = $(this).val();
    if (this.type === "radio" || this.type === "checkbox") {
        //do nothing at the moment
    }
    else {
        if (value.length !== 0) {
            if (this.type === "text") {
                if (value.length < 200) {
                    BlurSuccess(this.id);
                }
                else {
                    BlurFailure(this.id);
                }
            }
            if (this.type === "textarea") {
                if (value.length < 1000) {
                    BlurSuccess(this.id);
                }
                else {
                    BlurFailure(this.id);
                }
            }

            if (this.type === "email") {
                RegCheck(this.id, value, /^[^\s@]+@[^\s@]+\.[^\s@]+$/, 254);
            }
            if (this.type === "tel") {
                RegCheck(this.id, value, /([0-9\s\-]{7,})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/, 50);
            }
            if (this.type === "number") {
                RegCheck(this.id, value, /^\d*$/, 100);
            }
        }
        if (value.length === 0) {
            if (!$(this).prop('required')) {
                BlurSuccess(this.id);
            }
            else {
                BlurFailure(this.id);
            }
        }
    }
});