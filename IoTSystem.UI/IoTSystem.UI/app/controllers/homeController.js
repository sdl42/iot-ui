﻿app.controller("homeController", function ($scope, $http, $q, storage, $window, loading) {
    $scope.courses = [];
    $scope.search = "";

    token = storage.get("current_token");

    var config = {
        headers: {
            "token": token
        }
    };

    $scope.courseImg = function () {
        var arrayInc = 0;
        var images = ["developer.jpg", "digital_marketing.jpg", "infrastructure.jpg", "network.jpg"];

        for (var i = 0; i < $scope.courses.length; i++) {
            if (arrayInc > 3) {
                arrayInc = 0;
            }

            $scope.courses[i].img = images[arrayInc];
            arrayInc++
        }
    }

    var q = $q.defer();
 
    $scope.getCourses = function () {
        $http.get(server_url + '/courses/get')
            .then(function (successCallback) {
                if (successCallback.status == 200) {
                    console.log(successCallback.data);
                    $scope.OrignalCourses = successCallback.data;
                    $scope.courses = successCallback.data;
                    $scope.courseImg();

                }
            }, function (errorCallback) {
                if (errorCallback.status !== 200) {
                    alert("an error occured");

                }
            });
    }

    $scope.checkLogin = function () {
        
        $http.get(server_url + '/users/details', config)
            .then(function (successCallback) {
                if (successCallback.status == 200) {
                    $window.location.href = '/student';


                }
            }, function (errorCallback) {
                    if (errorCallback.status !== 200) {
                        q.resolve("not logged in");
                }

            });

        return q.promise;

    }
    
    var promise = $scope.checkLogin();
    loading.start();

    promise.then(function (result) {
        $scope.getCourses()
        loading.stop();
    });


    $scope.filterData = function () {
        var tempCourses = [];

        for (var x = 0; x < $scope.OrignalCourses.length; x++) {
            if ($scope.OrignalCourses[x].name.toLowerCase().indexOf($scope.search.toLowerCase()) !== -1) {
                tempCourses.push($scope.OrignalCourses[x]);
            }
            else if ($scope.OrignalCourses[x].description.toLowerCase().indexOf($scope.search.toLowerCase()) !== -1) {
                tempCourses.push($scope.OrignalCourses[x]);
            }
        }
        $scope.courses = tempCourses;

    }

    

    var courseCounter = 0;
    $scope.toggleBtn = function(divID) {
        $('#' + divID).toggleClass('btn-primary');
        $('#' + divID).toggleClass('btn-secondary');
        var btnText = document.getElementById(divID).innerHTML;
        if (btnText == "Interested!") {
            btnText = "Add to interests?";
            courseCounter--;
        } else {
            btnText = "Interested!"
            courseCounter++;
        }
        document.getElementById(divID).innerHTML = btnText;

        if (courseCounter > 0) {
            promptText = "Register for your courses.";
        } else {
            promptText = "(Not logged in)"
        }
        $('.title').text(promptText);
    }



});