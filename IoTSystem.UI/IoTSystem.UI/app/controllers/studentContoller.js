﻿app.controller("studentController", function ($scope, storage, $http, user_details, $q, $location, $window, loading) {
    token = storage.get("current_token");
    storage.set("last_page", "student")

    var config = {
        headers: {
            "token": token
        }
    };

    $scope.current_user = {};
    $scope.courses = [];
    $scope.registered = [];
    $scope.search = "";

    var url_query = $location.search()

    if (url_query.courses !== undefined) {
        $scope.courseQuery = url_query.courses;
    }
    else {
        $scope.courseQuery = "all";
    }

    $scope.returnedCourses = false;
    $scope.returnedRegistered = false;

    $scope.courseImg = function () {
        var arrayInc = 0;
        var images = ["developer.jpg", "digital_marketing.jpg", "infrastructure.jpg", "network.jpg"];

        for (var i = 0; i < $scope.courses.length; i++) {
            if (arrayInc > 3) {
                arrayInc = 0;
            }
            
            $scope.courses[i].img = images[arrayInc];
            arrayInc++
        }
    }


    $scope.getAllCourses = function () {
        $http.get(server_url + '/users/registered-courses', config)
            .then(function (successCallback) {
                if (successCallback.status == 200) {
                    $scope.registered = successCallback.data;
                    $scope.returnedRegistered = true;
                    $scope.registeredMatch();
                }
            }, function (errorCallback) {
                    if (errorCallback.status == 404) {
                        $scope.registered = null;
                        console.log("error no registered courses");
                        $scope.registeredMatch();
                    }
            });

        $http.get(server_url + '/courses/get', config)
            .then(function (successCallback) {
                if (successCallback.status == 200) {
                    $scope.courses = successCallback.data;
                    $scope.OrignalCourses = successCallback.data;
                    $scope.returnedCourses = true;
                    $scope.courseImg();
                    $scope.registeredMatch();
                }
            }, function (errorCallback) {
                    if (errorCallback.status !== 200) {
                        alert("an error occured");
                        $scope.returnedCourses = true;
                        $scope.registeredMatch();
                    }
            });
    }

    $scope.getMyCourses = function () {
        $http.get(server_url + '/users/registered-courses', config)
            .then(function (successCallback) {
                if (successCallback.status == 200) {
                    $scope.courses = successCallback.data;
                    $scope.OrignalCourses = successCallback.data;
                    
                }
            }, function (errorCallback) {
                if (errorCallback.status == 404) {
                    $scope.registered = null;
                    console.log("error no registered courses");
                }
            });

    }

    if ($scope.courseQuery == "all") {
        loading.start();
        var promise = user_details.get();
        
        promise.then(function (result) {
            $scope.current_user = result
            $scope.current_user.fullName = $scope.current_user.firstName + " " + $scope.current_user.secondName;
            $scope.getAllCourses();
            loading.stop();
        });
    }
    if ($scope.courseQuery == "my") {
        loading.start();
        var promise = user_details.get();

        promise.then(function (result) {
            $scope.current_user = result
            $scope.current_user.fullName = $scope.current_user.firstName + " " + $scope.current_user.secondName;
            $scope.getMyCourses()
            loading.stop();
        });
    }
    

    $scope.registeredMatch = function () {

        if ($scope.returnedRegistered && $scope.returnedCourses) {
            for (var x = 0; x < $scope.courses.length; x++) {
                $scope.courses[x].enrolled = false;
                for (var y = 0; y < $scope.registered.length; y++) {
                    if ($scope.courses[x].id === $scope.registered[y].courseId) {
                        $scope.courses[x].enrolled = true;
                        $scope.courses[x].enrolledState = $scope.registered[y].state;
                    }
                }
            }
        }

    }


    $scope.registerInterest = function (courseID) {
        var querystring = "?enrollState=0&roleType=0"
        var course = [];
        course.push(courseID);
        console.log(courseID);
        $http.post(server_url + '/courses/enroll-user-on-course' + querystring, course, config)
            .then(function (successCallback) {
                if (successCallback.status == 200) {
                    $window.location.href = '/student';
                }
            }, function (errorCallback) {
                if (errorCallback.status == 404) {
                    console.log("error");
                }
            });
    }


    $scope.filterData = function () {
        var tempCourses = [];

        for (var x = 0; x < $scope.OrignalCourses.length; x++) {
            if ($scope.OrignalCourses[x].name.toLowerCase().indexOf($scope.search.toLowerCase()) !== -1) {
                tempCourses.push($scope.OrignalCourses[x]);
            }
            else if ($scope.OrignalCourses[x].description.toLowerCase().indexOf($scope.search.toLowerCase()) !== -1) {
                tempCourses.push($scope.OrignalCourses[x]);
            }
        }
        $scope.courses = tempCourses;
        
    }

    $scope.logout = function () {
        user_details.logout();
    }



});