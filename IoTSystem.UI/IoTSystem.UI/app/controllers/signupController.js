﻿app.controller("signupController", function ($scope, $http, $localStorage, storage, $window) {
    $scope.secondPass = "";

    $scope.signup = {};


    $scope.signupSubmit = function () {
        var valid = true;
        var full = $scope.signup.firstName.split(" ");

        $scope.signup.firstName = full[0];
        $scope.signup.secondName = full[1];
        console.log($scope.signup);


        if ($scope.signup.password !== $scope.secondPass) {
            alert("Passowrds don't match");
            valid = false;
        }
        else {
            valid = true;
        }

        if (valid) {
            $http.post(server_url + "/users/signup", $scope.signup)
                .then(function (response) {
                    if (response.status !== 200) {
                        alert("An error occured");
                    }
                    else {
                        $window.location.href = '/login';
                    }
                    
                })
        }

    }
});