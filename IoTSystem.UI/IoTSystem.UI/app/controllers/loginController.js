﻿app.controller("loginController", function ($scope, $http, storage, $window, loading) {
    $scope.login = [];
    $scope.resetID = undefined;

    var invalid = false;

    $scope.loginSubmit = function () {
        loading.start();
        var querystring = "?id=" + $scope.login.id + "&password=" + $scope.login.password;
        $http.get(server_url + '/users/login' + querystring).then(function (successCallback) {
            if (successCallback.status == 200) {
                storage.set("current_token", successCallback.data.token);
                storage.set("current_role", successCallback.data.userRoleValue);

                if (successCallback.data.userRoleValue == 0) {
                    $window.location.href = '/student';
                }
                if (successCallback.data.userRoleValue == 25) {
                    $window.location.href = '/teacher';
                }
                if (successCallback.data.userRoleValue == 50) {
                    $window.location.href = '/admin';
                }
            }
        }, function (errorCallback) {
                if (!invalid) {
                    var label = '<label style="color:red;padding:10px;">Incorrect id or password</label>';
                    $('#passwordField').append(label);
                }
                loading.stop();
                invalid = true;
        });
    }

    $scope.ResetPassword = function () {
        $(".popup_form").slideToggle();
        $(".blur").toggle();
    }

    $scope.ResetSubmit = function () {
        $http.patch(server_url + "/users/forgot-password?id=" + $scope.resetID)
            .then(function (successCallback) {
                if (successCallback.status == 200) {
                    $scope.ResetPassword();
                }
            }, function (errorCallback) {
                if (errorCallback.status == 404) {
                    alert("error");
                    $window.location.href = '/edit';
                }
            });
    }

});


