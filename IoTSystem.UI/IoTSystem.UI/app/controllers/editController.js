﻿app.controller("editController", function ($scope, $http, storage, $window, user_details, $q, loading) {
    $scope.edit = [];
    $scope.password = "";
    $scope.confirmPassword = "";
    $scope.lastpage = "/" + storage.get("last_page");

    token = storage.get("current_token");

    var config = {
        headers: {
            "token": token
        }
    };

    loading.start();
    var promise = user_details.get();

    promise.then(function (result) {
        $scope.edit = result
        console.log(result);
        loading.stop();
    });

    $scope.editSubmit = function () {

        $scope.edit.contactId = $scope.edit.id;
        delete $scope.edit.id
        delete $scope.edit.firstName
        delete $scope.edit.secondName

        $http.patch(server_url + "/users/edit", $scope.edit, config)
            .then(function (successCallback) {
                if (successCallback.status == 200) {
                    $window.location.href = '/edit';
                }
            }, function (errorCallback) {
                if (errorCallback.status == 404) {
                    alert("error");
                    $window.location.href = '/edit';
                }
            });
    }

    $scope.ChangePassword = function () {
        $(".popup_form").slideToggle();
        $(".blur").toggle();
    }

    $scope.PasswordSubmit = function () {
        var valid = false;
        if ($scope.password === $scope.confirmPassword) {
            $http({
                method: 'PATCH',
                url: server_url + "/users/edit-password?newPassword=" + $scope.password,
                headers: {
                    "Content-Type": "application/json-path+json",
                    "token": token
                }
            }).then(function (response) {
                if (response.status == '200') {
                    $scope.ChangePassword();
                } else {
                    alert("error");
                    $window.location.href = '/edit';
                }
            });
        }
        else {
            alert("passwords do not match");
        }
    }

    $scope.logout = function() {
        user_details.logout();
    }
});