﻿app.controller("enrollController", function ($scope, $http, $location, $window) {

    $scope.toggleBtn =  function () {
        $('#register').toggleClass('register-hide');
    }
    $scope.enroll = {};
    $scope.name = "Name";

    var url_query = $location.search()

    if (url_query.email !== undefined){
        $scope.enroll.emailAddress = url_query.email;
    }
    if (url_query.name !== undefined) {
        $scope.name = url_query.name;
    }
    
    $scope.enrollSubmit = function () {
        var valid = true;

        var regex = /([0-9\s\-]{7,})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/

        if (!regex.test($scope.enroll.contactNumber)) {
            alert("not correct number format");
            valid = false;
        }
        else {
            valid = true;
        }
        console.log($scope.enroll);
        if (valid) {
            $http.patch(server_url + "/users/signup-details", $scope.enroll)
                .then(function (response) {
                    if (response.status !== 200) {
                        alert("An error occured");
                    }
                    else {
                        console.log(response.data);
                        $window.location.href = '/login';
                    }

                })
        }

    }
});