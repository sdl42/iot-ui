﻿app.controller("adminController", function ($scope, $http, $location, storage, loading, user_details, $window) {


	var role = storage.get("current_role");
	if (role !== 50) {
		if (role === 0) {

			$window.location.href = '/student';
        }
		if (role === 25) {

			$window.location.href = '/teacher';
        }
	}

	$scope.courseImg = function () {
		var arrayInc = 0;
		var images = ["developer.jpg", "digital_marketing.jpg", "infrastructure.jpg", "network.jpg"];

		for (var i = 0; i < $scope.courses.length; i++) {
			if (arrayInc > 3) {
				arrayInc = 0;
			}

			$scope.courses[i].img = images[arrayInc];
			arrayInc++
		}
	}

	$scope.addCourse = {};
	$scope.addAdmin = {};
	$scope.current_user = {};

	loading.start();
	var promise = user_details.get();

	promise.then(function (result) {
		$scope.current_user = result
		$scope.current_user.fullName = $scope.current_user.firstName + " " + $scope.current_user.secondName;
		loading.stop();
	});

	var img = ["developer.jpg", "digital_marketing.jpg", "infrastructure.jpg", "network.jpg"];
	$scope.randimg = function () {
		return img[Math.floor(Math.random() * img.length)];
	}


	$scope.openForm = function () {
		document.getElementById("bookRoom").style.display = "block";
		document.getElementById("openButton").style.display = "none";
	}

	$scope.closeForm = function () { //Closes booking room form
		document.getElementById("bookRoom").style.display = "none";
		document.getElementById("openButton").style.display = "block";
	}

	$scope.userMenu = function () {
		$('#menu').slideToggle();

	}

	$scope.studentID = null;

	token = storage.get("current_token");
	storage.set("last_page", "teacher")

	var config = {
		headers: {
			"token": token
		}
	};

	function isInteger(value) {
		if (parseInt(value, 10).toString() === value) {
			return true
		}
		return false;
	}

	var url_query = $location.search();

	$scope.tabSwitch = function (tab) {
		$scope.tab = tab;

		if (tab === "courses") {
			if ($scope.lastTab !== $scope.tab) {
				$('#courses').slideToggle();
				$("#" + $scope.lastTab).slideToggle();
				$scope.lastTab = "courses";
			}

			$scope.courses = [];

			$http.get(server_url + '/courses/get')
				.then(function (successCallback) {
					if (successCallback.status == 200) {
						$scope.courses = successCallback.data;
						$scope.OrignalCourses = successCallback.data;
						$scope.courseImg();

					}
				}, function (errorCallback) {
					if (errorCallback.status !== 200) {
						alert("an error occured");
					}
				});

			$scope.filterData = function () {
				var tempCourses = [];

				for (var x = 0; x < $scope.OrignalCourses.length; x++) {
					if ($scope.OrignalCourses[x].name.toLowerCase().indexOf($scope.search.toLowerCase()) !== -1) {
						tempCourses.push($scope.OrignalCourses[x]);
					}
					else if ($scope.OrignalCourses[x].description.toLowerCase().indexOf($scope.search.toLowerCase()) !== -1) {
						tempCourses.push($scope.OrignalCourses[x]);
					}
				}
				$scope.courses = tempCourses;

			}

			$scope.addCourseSubmit = function () {
				console.log($scope.addCourse);
				$http.post(server_url + '/admin/add-course', $scope.addCourse, config)
					.then(function (successCallback) {
						if (successCallback.status == 200) {
							$window.location.href = '/admin';
						}
					}, function (errorCallback) {
						if (errorCallback.status !== 200) {
							alert("an error occured");
						}
					});
			}

			$scope.toggleAddCourse = function () {
				$(".blur").toggle();
				$(".popup_form").slideToggle();
			}
		}
		if (tab === "students") {

			if ($scope.lastTab !== $scope.tab) {
				$('#students').slideToggle();
				$("#" + $scope.lastTab).slideToggle();
				$scope.lastTab = "students";
			}

			$scope.students = [];

			$http.get(server_url + '/teachers/all-students', config)
				.then(function (successCallback) {
					if (successCallback.status == 200) {
						$scope.students = successCallback.data;
						$scope.OriginalStudents = successCallback.data;
					}
				}, function (errorCallback) {
					if (errorCallback.status !== 200) {
						alert("an error occured");
					}
				});

			$scope.filterData = function () {

				var tempStudents = [];

				loading.start();

				if (isInteger($scope.search)) {
					for (var x = 0; x < $scope.OriginalStudents.length; x++) {
						if ($scope.OriginalStudents[x].id === parseInt($scope.search)) {
							tempStudents.push($scope.OriginalStudents[x]);
						}
					}
				}
				else {
					for (var x = 0; x < $scope.OriginalStudents.length; x++) {
						if ($scope.OriginalStudents[x].firstname.toLowerCase().indexOf($scope.search.toLowerCase()) !== -1) {
							tempStudents.push($scope.OriginalStudents[x]);
						}
						else if ($scope.OriginalStudents[x].secondname.toLowerCase().indexOf($scope.search.toLowerCase()) !== -1) {
							tempStudents.push($scope.OriginalStudents[x]);
						}
					}
				}

				loading.stop();
				$scope.students = tempStudents;
			}

		}
		if (tab === "timetable") {
			if ($scope.lastTab !== $scope.tab) {
				$('#timetable').slideToggle();
				$("#" + $scope.lastTab).slideToggle();
				$scope.lastTab = "timetable";
			}
		}
		if (tab === "studentView") {
			if ($scope.lastTab !== $scope.tab) {
				$('#studentView').slideToggle();
				$("#" + $scope.lastTab).slideToggle();
				$scope.lastTab = "studentView";
			}

			$scope.stu = [];

			$http.get(server_url + '/teachers/student-on-course?id=' + $scope.studentID, config)
				.then(function (successCallback) {
					if (successCallback.status == 200) {
						$scope.stu = successCallback.data;
					}
				}, function (errorCallback) {
					if (errorCallback.status !== 200) {
						alert("an error occured");
					}
				});

			$scope.update = function (usercourse) {
				$http({
					method: 'POST',
					url: server_url + "/admin/update-state?userCourseId=" + usercourse + "&enrollState=1",
					headers: {
						"Content-Type": "application/json-path+json",
						"token": token
					}
				}).then(function (response) {
					if (response.status == '200') {
						$scope.tabSwitch("studentView");
					} else {
						alert("error");
					}
				});
			}

		}
		if (tab === "roomBooking") {
			if ($scope.lastTab !== $scope.tab) {
				$('#roomBooking').slideToggle();
				$("#" + $scope.lastTab).slideToggle();
				$scope.lastTab = "roomBooking";
			}
		}
		if (tab === "addAdmin") {
			$scope.password2 = "";


			if ($scope.lastTab !== $scope.tab) {
				$('#addAdmin').slideToggle();
				$("#" + $scope.lastTab).slideToggle();
				$scope.lastTab = "addAdmin";
			}

			$scope.addAdminSubmit = function () {
				if ($scope.addAdmin.password === $scope.password2) {
					console.log($scope.addAdmin);
					$http.post(server_url + '/admin/add', $scope.addAdmin, config)
						.then(function (successCallback) {
							if (successCallback.status == 200) {
								alert("user successfully added");
								$scope.tabSwitch("addAdmin");
							}
						}, function (errorCallback) {
							if (errorCallback.status !== 200) {
								alert("an error occured");
							}
						});

				}
				else {
					alert("passwords do not match, please try again")
                }

			}
		}
	}

	$('#courses').slideToggle();
	$scope.tab = "courses";
	$scope.lastTab = "courses";
	$('#coursesTab').addClass("active");

	$scope.tabSwitch("courses");


	$scope.studentSwitch = function (id) {
		$scope.studentID = id;
		$scope.tabSwitch("studentView");
	}

	$scope.logout = function () {
		user_details.logout();
	}

});