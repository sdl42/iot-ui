﻿app.controller("resetController", function ($scope, $http, $window, loading, $location) {
    $scope.newPassword = "";
    $scope.newPassword2 = "";
    $scope.id = null;

    token = $location.search().Token;

    console.log(token);

    var config = {
        headers: {
            "resetToken": token
        }
    };

    $scope.resetSubmit = function () {
        if ($scope.newPassword === $scope.newPassword2) {
            $http.delete(server_url + "/users/forgot-password-submit?id=" + $scope.id + "&newPassword=" + $scope.newPassword, config)
                .then(function (successCallback) {
                    if (successCallback.status == 200) {
                        alert("success");
                        //$window.location.href = '/edit';
                    }
                }, function (errorCallback) {
                    if (errorCallback.status != 200) {
                        alert("error");
                        //$window.location.href = '/edit';
                    }
                });
        }
        else {
            alert("Error: Passwords don't match. Please try again");

        }


    }

});