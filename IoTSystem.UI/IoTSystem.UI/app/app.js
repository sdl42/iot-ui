﻿//var app = angular.module("testApp", ["ngRoute"]);
var app = angular.module("Main", ["ngRoute", 'ngStorage']);

var server_url = "https://iotsystemapi.azurewebsites.net";

app.config(function ($routeProvider, $locationProvider) {


    $locationProvider.hashPrefix('');
    $locationProvider.html5Mode(true);
    $routeProvider
        .when("/", {
            templateUrl: "app/views/home/index.html",
            controller: "homeController"
        })
        .when("/signup", {
            templateUrl: "app/views/signup/index.html",
            controller: "signupController"
        })
        .when("/login", {
            templateUrl: "app/views/login/index.html",
            controller: "loginController"
        })
        .when("/employer", {
            templateUrl: "app/views/employer/index.html",
            controller: "employerController"
        })
        .when("/teacher", {
            templateUrl: "app/views/teacher/index.html",
            controller: "teacherController"
        })
        .when("/admin", {
            templateUrl: "app/views/admin/index.html",
            controller: "adminController"
        })
        .when("/student", {
            templateUrl: "app/views/student/index.html",
            controller: "studentController"
        })
        .when("/enroll", {
            templateUrl: "app/views/enroll/index.html",
            controller: "enrollController"
        })
        .when("/edit", {
            templateUrl: "app/views/edit/index.html",
            controller: "editController"
        })
        .when("/forgotpassword", {
            templateUrl: "app/views/reset/index.html",
            controller: "resetController"
        })
        .otherwise({ redirectTo: '/' });


});



