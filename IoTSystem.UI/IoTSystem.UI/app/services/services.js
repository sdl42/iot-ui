
app.service('storage', function ($http, $localStorage) {
    this.set = function (key, value) {
        $localStorage[key] = value;
    }
    this.get = function (key) {
        if ($localStorage[key] === undefined) {
            $localStorage[key] = "";
        }
        return $localStorage[key];
    }
});



app.service('user_details', function ($http, storage, $window, $q) {

    token = storage.get("current_token");
    var config = {
        headers: {
            "token": token
        }
    };

    var q = $q.defer();
    var data = [];

    this.get = function () {
        $http.get(server_url + '/users/details', config)
            .then(function (successCallback) {
                if (successCallback.status == 200) {
                    data = successCallback.data;
                    q.resolve(data.pop());

                }
            }, function (errorCallback) {
                    if (errorCallback.status !== 200) {
                        $window.location.href = '/login';
                        //q.reject("data");
                    }
                
            });
        
        return q.promise;
        
    };

    this.logout = function () {
        storage.set("current_token", "");
        $window.location.href = '/login';
    };

});

app.service('loading', function () {
    this.start = function() {
        $('.loading_box').show();
    }
    this.stop = function() {
        $('.loading_box').hide();
    }
   
});